/**
 * Created by 97974 on 2016/5/27.
 */
(function($) {
    $.fn.extend({
        //首页banner插件
        Tab: function(options) {
            //设置默认值
            var defaults={
                handle:'.content-nav',
                tab:'.tab',
                tabWrap:'.tab-wrap',
                tabItem:'.tab-item',
                next:'.next',
                prev:'.prev',
                index:0
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var count=$(o.handle).children().length;
                var wid=null;
                var iwid=$(o.tab).width();
                var idx=o.index;
                for(var i=0;i<count;i++){
                    $(o.tabItem).eq(i).width(iwid)
                    wid+=$(o.tabItem).width();
                }
                function change(i){
                    $(o.tabWrap).css('left',-iwid*idx);
                    $(o.handle).children().eq(i).addClass("active").siblings().removeClass("active");
                }
                $(o.tabWrap).width(iwid*count);
                $(".tablew").height($(o.tab).height()-20)
                change(idx);
                function clickChange(i){
                    if(i>=0 && idx<count-1){
                        idx++;
                    }
                    else if(i<0 && idx>0){
                        idx--;
                    }
                    change(idx)
                }
                $(o.handle).children().click(function(){
                    idx=$(this).index();
                    change(idx);
                })
                $(o.next).click(function(){
                    clickChange(0);
                })
                $(o.prev).click(function(){
                    clickChange(-1);
                })
            });
        },
        Scroll:function(){
            //设置默认值
            var defaults={
                dom:'.txtMarquee-top .bd',
                wrap:".infoList",
                pause:".scrollBtn",
                auto:false
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var obj=$(o.dom);
                var timer=null;
                var ul=obj.find(o.wrap).html(obj.find(o.wrap).html()+obj.find(o.wrap).html())
                $(o.wrap).css("position","relative");
                var i=0;
                function move(){
                    var tops=$(o.wrap).scrollTop()-i;
                    i++
                    if(i>=$(o.dom).height()){
                        i=0;
                    }
                    $(o.wrap).css({"top":tops})
                }
                $(o.pause).click(function(e){
                    e.stopPropagation();
                    o.auto=!o.auto;
                    if(!o.auto){
                        clearInterval(timer)
                        $(o.pause).removeClass("active")
                    }else{
                        clearInterval(timer)
                        timer=setInterval(move,50)
                        $(o.pause).addClass("active")
                    }
                })
            });
        },
        Banner: function(options) {
            //设置默认值
            var defaults={
                index:0,
                speed:100,
                delay:4000,
                effect:"linner",
                item:".banner-item",
                phoneImg:["img/banner.jpg","img/banner.jpg","img/banner.jpg","img/banner.jpg","img/banner.jpg"],
                tabBg:"img/banner.jpg"//如果背景不变的话，直接填字符串，如果多张背景切换，则填数组
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var obj=$(this);
                var items=$(o.item,obj);
                var phone=$(window).width()<=600?true:false;
                function init(){
                    if(phone){
                        items.width($(window).width())
                        items.each(function(i,ele){
                            $(this).html('<img src='+o.phoneImg[i]+'>')
                        })
                        o.delay=3000;
                        obj.width(items.width()*items.length).addClass("phone");
                    }else{
                        if(typeof o.tabBg=="string"){
                            if(o.tabBg.search("#")==0){
                                obj.css("background-color",o.tabBg);
                            }else{
                                obj.css("background-image","url("+o.tabBg+")");
                            }
                            items.each(function(index,ele){
                                var itemobj=$(".animated",$(this));
                                itemobj.hide()
                            })
                        }else{
                            items.each(function(index,ele){
                                var bg="url("+o.tabBg[index]+")";
                                $(this).css("background-image",bg);
                                var itemobj=$(".animated",$(this));
                                itemobj.hide()
                            })
                        }
                    }
                }
                init();
                function change(i){
                    if(phone){
                        obj.css("margin-left",-$(window).width()*o.index)
                    }else{
                        items.eq(i).fadeIn(o.speed).siblings().fadeOut(o.speed);
                        var itemc=$(".animated",items.eq(i));
                        var sibitem=$(".animated",items.eq(i).siblings());
                        itemc.each(function(index,ele){
                            var _this=$(this);
                            setTimeout(function(){
                                _this.fadeIn("fast").addClass(_this.attr("entry"))
                            },$(this).attr("delay"))
                            setTimeout(function(){
                                _this.removeClass(_this.attr("entry")).addClass(_this.attr("leave"))
                            },o.delay-_this.attr("delay")-500)
                            setTimeout(function(){
                                _this.removeClass(_this.attr("leave")).hide()
                            },o.delay)
                        })
                    }
                }
                change(o.index)
                setInterval(function(){
                    if(o.index<items.length-1){
                        o.index++;
                    }else{
                        o.index=0;
                    }
                    change(o.index)
                },o.delay)
            });
        },
        Pop:function(options){
            //设置默认值
            var defaults={
                content:'',
                margintop:20,
                top:45,
                effect:"click",
                rect:[100,100]
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                $(this).bind(options.effect,function(e){
                    e.stopPropagation();
                    var left=$(this).offset().left;
                    var top= e.clientY;
                    var height=e.delegateTarget.clientHeight;
                    var width=e.delegateTarget.clientWidth;
                    $(".pop").remove()
                    $("<div class='pop'>"+options.content+"<span class='popa'></span></div>")
                        .css({
                            position:"fixed",
                            left:left+width/2,
                            top:options.top || top+height+options.margintop,
                            zIndex:110,
                            width: o.rect[0],
                            height: o.rect[1]
                        })
                        .appendTo($(document.body));
                    var popw=$(".pop").width() || o.rect[0];
                    var poph=$(".pop").height() || o.rect[1];
                    var popl=$(".pop").offset().left;
                    if(left+popw/2>$(window).width()){
                        $(".pop").css({
                            left:"auto",
                            right:10
                        })
                    }else{
                        $(".pop").css({
                            left:popl-popw/2
                        })
                    }
                    $(".popa").css({
                        position:"fixed",
                        top:options.top-10 || top+height-options.margintop,
                        left:left+width/2-10,
                        zIndex:110
                    })
                })
                $(document).bind("click",function(){
                    $(".pop").remove()
                })
            });
        },
        /*Slider:function(options){
            //设置默认值
            var defaults={
                wrap:".slide-wrap",
                item:".slide-item",
                box:".slide-box",
                vis:5,
                step:1,
                speed:2000,
                loop:true,
                ion:true
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var index=0;
                var ion=null;
                var _this=$(this);
                var objw=_this.width();
                var itemW=objw/ o.vis;
                var length=_this.find(o.item).length;
                _this.find(o.item).width(itemW);
                var wrap=_this.find(o.wrap).width(itemW*length);
                if(o.loop){
                    $(o.box).after($(o.box).clone(true))
                    wrap.width(itemW*length*2)
                }
                var timer=setInterval(step, o.speed)
                function step (){
                    if(o.loop){
                        if(index<length){
                            index++;
                        }else{
                            index=1;
                            reback();
                        }
                    }else{
                        if(index<length-o.vis){
                            index++;
                        }else{
                            index=0;
                        }
                    }
                    move(index);
                }
                var on= o.ion?addIon():null;
                function addIon(){
                    ion=$('<div class="slide-ion"></div>').appendTo(_this);
                    for(var i=0;i<Math.ceil(length/ o.vis);i++){
                        var btn=$('<span></span>').appendTo(ion)
                        if(i==0){
                            btn.addClass("active")
                        }
                    }
                }
                wrap.mouseover(function(){
                    clearInterval(timer)
                })
                wrap.mouseout(function(){
                    clearInterval(timer)
                    timer=setInterval(step, o.speed)
                })
                function move(index){
                    /!*wrap.css({left:-index*itemW*o.step});*!/
                    wrap.animate({left:-index*itemW* o.step},300)
                    if(index%o.vis){
                        var ionIndex=Math.ceil(index/o.vis)-1;
                    }else{
                        index=index+1;
                        var ionIndex=Math.ceil(index/o.vis)-1;
                    }
                    console.log(index,ionIndex)
                    ion.children().eq(ionIndex).addClass("active").siblings().removeClass("active")
                }
                function reback(){
                    wrap.css({left:0});
                }
            });
        },*/
        Slider:function(options){
            //设置默认值
            var defaults={
                wrap:".slide-wrap",
                item:".slide-item",
                box:".slide-box",
                vis:5,
                step:1,
                speed:2000,
                loop:true,
                ion:true
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var _this=$(this);
                var index=0;
                var length=_this.find(o.item).length;
                var itemH=_this.find(o.item).height();
                var wrap=_this.find(o.wrap).height(itemH);
                var box=_this.find(o.box).height(itemH*(index/ o.vis));
                var ions=null;
                console.log(itemH)
                var timer=setInterval(step, o.speed);
                var ion= o.ion?addIon():null;
                _this.mouseover(function(){
                    clearInterval(timer);
                })
                _this.mouseout(function(){
                    clearInterval(timer);
                    timer=setInterval(step, o.speed);
                })
                function step(){
                    if(index<length/ o.vis-1){
                        index++;
                    }else{
                        index=0;
                    }
                    move(index)
                }
                function move(index){
                    var ionIndex=index;
                    ions.children().eq(ionIndex).addClass("active").siblings().removeClass("active");
                    box.animate({top:-index*itemH},300)
                }
                function addIon(){
                    ions=$('<div class="slide-ion"></div>').appendTo(_this);
                    for(var i=0;i<Math.ceil(length/ o.vis);i++){
                        var btn=$('<span></span>').appendTo(ions)
                        if(i==0){
                            btn.addClass("active")
                        }
                    }
                }
            });
        },
        Scroll:function(options){
            //设置默认值
            var defaults={
                className:"",
                y:0,
                fun1:null,
                fun2:null
            }
            var options=$.extend(defaults,options);
            //遍历匹配元素的集合
            return this.each(function() {
                var o=options;
                var obj=$(this);
                var target=false;
                $(window).scroll(function(event){
                    var top=GetPageScroll().Y;
                    if(top>=o.y){
                        obj.addClass(o.className);
                        if(!target){
                            o.fun1();
                            target=true;
                        }
                    }else{
                        obj.removeClass(o.className)
                        o.fun2();
                    }
                })
            });
        }
    });
})(jQuery);

function GetPageScroll(){
    var x, y;
    if(window.pageYOffset){    // all except IE
        y = window.pageYOffset;
        x = window.pageXOffset;
    } else if(document.documentElement && document.documentElement.scrollTop) {    // IE 6 Strict
        y = document.documentElement.scrollTop;
        x = document.documentElement.scrollLeft;
    } else if(document.body) {    // all other IE
        y = document.body.scrollTop;
        x = document.body.scrollLeft;
    }
    return {X:x, Y:y};
}